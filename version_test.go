package slurm

import (
	"testing"
	"fmt"
)

func TestVersion(t * testing.T) {
	var v int32 = ApiVersion()
	fmt.Printf("SLURM API version: %d\n", v)
	major, minor, micro := ApiVersionDetails()
	fmt.Printf("SLURM API version: %d, %d, %d\n", major, minor, micro)
	if v <= 0 {
		t.Errorf("Version (%d) must be positive, to say the least", v)
	}
}

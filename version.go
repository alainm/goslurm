package slurm
// #cgo LDFLAGS: -lslurm
// #include <slurm/slurm.h>
// long slurm_version_major(long v) { return SLURM_VERSION_MAJOR(v); }
// long slurm_version_minor(long v) { return SLURM_VERSION_MINOR(v); }
// long slurm_version_micro(long v) { return SLURM_VERSION_MICRO(v); }
import "C"

func ApiVersion() int32 {
	return int32(C.slurm_api_version())
}

func ApiVersionDetails() (major, minor, micro int32) {
	version := C.slurm_api_version()
	major = int32(C.slurm_version_major(version))
	minor = int32(C.slurm_version_minor(version))
	micro = int32(C.slurm_version_micro(version))
	return
}

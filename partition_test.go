package slurm

import (
	"testing"
	"fmt"
)

func TestLoadPartition(t * testing.T) {
	fmt.Printf("### Testing Partitions\n")
	partitions, err := LoadPartitions()
	defer partitions.Close()
	defer fmt.Printf("### Partitions tested\n")
	if err != nil {
		t.Errorf("Could not load partitions: %s\n", err.Error())
		return
	} 
	fmt.Printf("Last update: %v\n", partitions.LastUpdate)
	for i, p := range partitions.Partitions {
		fmt.Printf("Partition %d: %s\n", i, p.Name)
		fmt.Printf(" Nodes: '%s'\n", p.Nodes)
		if p.AllowAllocNodes != "" {
			fmt.Printf(" AllowAllocNodes: '%s'\n", p.AllowAllocNodes)
		}
		if p.AllowAccounts != "" {
			fmt.Printf(" AllowAccounts: '%s'\n", p.AllowAccounts)
		}
	}
	partitions.Print("partitionsf.txt", false)
	partitions.Print("partitionst.txt", true)
}


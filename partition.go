package slurm
// #cgo LDFLAGS: -lslurm
// #include <slurm/slurm.h>
// #include <slurm/slurm_errno.h>
// #include <stdlib.h>
// #include <assert.h>
//
// partition_info_t *get_partition_info(partition_info_msg_t *msg, int i) {
//   return (i>=0 && i < msg->record_count) ? &(msg->partition_array[i]) : 0;
// }
// void dump_all(partition_info_msg_t *msg) {
// 	slurm_print_partition_info_msg(stdout, msg, 0);
// }
// void dump(partition_info_t *p) {
// 	slurm_print_partition_info(stdout, p, 0);
// }
// int partition_info_get_node_inx_sz(partition_info_t *part) {
//    int sz = 0;
//    while (part->node_inx[sz] != -1) {
//       ++sz;
//    }
//    return sz/2;
// }
// int32_t partition_info_get_node_inx(partition_info_t *part, int i) {
//    assert(partition_info_get_node_inx_sz(part)*2 > i);
//    return part->node_inx[i];
// }
import "C"
import "time"
import "errors"

type NodeRange struct {
	Start, End int
}

type PartitionInfo struct {
	// list names of allowed allocating nodes
	// char *allow_alloc_nodes;
	AllowAllocNodes string
	// comma delimited list of accounts, null indicates all
	// char *allowAccounts;
	AllowAccounts string
	// comma delimited list of groups, null indicates all
	// char *allowGroups;
	AllowGroups string
	// comma delimited list of qos, null indicates all
	// char *allowQos;
	AllowQos string
 	// name of alternate partition 
	// char *alternate;
	Alternate string
	// char *billingWeightsStr; /* per TRES billing weights string */
	BillingWeightsStr string
	// uint16T cr_type;	/* see CR_* values */
	CrType uint16
	// uint64_t def_mem_per_cpu; /* default MB memory per allocated CPU */
	DefMemPerCpu uint64
	// uint32_t default_time;	/* minutes, NO_VAL or INFINITE */
	DefaultTime uint32
	// char *deny_accounts;    /* comma delimited list of denied accounts */
	DenyAccounts string
	// char *deny_qos;		/* comma delimited list of denied qos */
	DenyQos string
	// uint16_t flags;		/* see PART_FLAG_* above */
	Flags uint16
	// uint32_t grace_time; 	/* preemption grace time in seconds */
	GraceTime uint32
	// uint32_t max_cpus_per_node; /* maximum allocated CPUs per node */
	MaxCpusPerNode uint32
	// uint64_t max_mem_per_cpu; /* maximum MB memory per allocated CPU */
	MaxMemPerCpu uint64
	// uint32_t max_nodes;	/* per job or INFINITE */
	MaxNodes uint32
	// uint16_t max_share;	/* number of jobs to gang schedule */
	MaxShare uint16
	// uint32_t max_time;	/* minutes or INFINITE */
	MaxTime uint32
	// uint32_t min_nodes;	/* per job */
	MinNodes uint32
	// char *name;		/* name of the partition */
	Name string
	// list index pairs into node_table Index 
	// * start_range_1, end_range_1,
	// * start_range_2, .., -1  
	// int32_t *node_inx;	
	NodeInx []NodeRange
	// char *nodes;		/* list names of nodes in partition */
	Nodes string
	// job's time limit can be exceeded by this
	// number of minutes before cancellation 
	// uint16_t over_time_limit; 
	OverTimeLimit uint16
	// uint16_t preempt_mode;	/* See PREEMPT_MODE_* in slurm/slurm.h */
	PreemptMode uint16
	// uint16_t priority_job_factor;	/* job priority weight factor */
	PriorityJobFactor uint16
	// uint16_t priority_tier;	/* tier for scheduling and preemption */
	PriorityTier uint16
	// char *qos_char;	        /* The partition QOS name */
	QosChar string
	// uint16_t state_up;	/* see PARTITION_ states above */
	StateUp uint16
	// uint32_t total_cpus;	/* total number of cpus in the partition */
	TotalCpus uint32
	// uint32_t total_nodes;	/* total number of nodes in the partition */
	TotalNodes  uint32
	// char    *tres_fmt_str;	/* str of configured TRES in partition */
	TresFmtStr string
}

type PartitionInfoMsg struct {
	impl *C.struct_partition_info_msg;
	LastUpdate time.Time
	Partitions []PartitionInfo
}

func (msg *PartitionInfoMsg) Close() {
	if msg.impl != nil {
		C.slurm_free_partition_info_msg(msg.impl)
	}
}

func (msg *PartitionInfoMsg) Print(fname string, oneLiner bool) {
	file := C.fopen(C.CString(fname), C.CString("a"))
	compact := 0
	if oneLiner {
		compact = 1
	}
	C.slurm_print_partition_info_msg(file, msg.impl, C.int(compact))
	C.fclose(file)
}

func (msg *PartitionInfoMsg) PrintPartition(fname string, i int, oneLiner bool) {
	file := C.fopen(C.CString(fname), C.CString("a"))
	compact := 0
	if oneLiner {
		compact = 1
	}
	C.slurm_print_partition_info(file, C.get_partition_info(msg.impl, C.int(i)), C.int(compact))
	C.fclose(file)
}

func LoadPartitions() (msg *PartitionInfoMsg, err error) {
	msg = new(PartitionInfoMsg)
	errcode := C.slurm_load_partitions(
		C.time_t(0),
		&msg.impl,
		0)
	if (errcode != 0) {
		C.slurm_perror(C.CString("GO SLURM problem"))
		err = errors.New(C.GoString(C.slurm_strerror(C.slurm_get_errno())))
	}
	msg.LastUpdate = time.Unix(int64(msg.impl.last_update), 0)
	msg.Partitions = make([]PartitionInfo, msg.impl.record_count)
	for i, _ := range msg.Partitions {
		cp := C.get_partition_info(msg.impl, C.int(i))
		p  := &msg.Partitions[i]
		p.AllowAllocNodes   = C.GoString(cp.allow_alloc_nodes)
		p.AllowAccounts     = C.GoString(cp.allow_accounts)
		p.AllowGroups       = C.GoString(cp.allow_groups)
		p.AllowQos          = C.GoString(cp.allow_qos)
		p.Alternate         = C.GoString(cp.alternate)
		p.BillingWeightsStr = C.GoString(cp.billing_weights_str)
		p.CrType = uint16(cp.cr_type)
		p.DefMemPerCpu = uint64(cp.def_mem_per_cpu)
		p.DefaultTime = uint32(cp.default_time)
		p.DenyAccounts = C.GoString(cp.deny_accounts)
		p.DenyQos = C.GoString(cp.deny_qos)
		p.Flags = uint16(cp.flags)
		p.GraceTime = uint32(cp.grace_time)
		p.MaxCpusPerNode = uint32(cp.max_cpus_per_node)
		p.MaxMemPerCpu = uint64(cp.max_mem_per_cpu)
 		p.MaxNodes = uint32(cp.max_nodes)
		p.MaxShare = uint16(cp.max_share)
		p.MaxTime = uint32(cp.max_time)
		p.MinNodes = uint32(cp.min_nodes)
		p.Name = C.GoString(cp.name)
		// NodeInx []int32
		nbNodeInx := int(C.partition_info_get_node_inx_sz(cp))
		p.NodeInx = make([]NodeRange, nbNodeInx)
		for i := 0; i < nbNodeInx; i++ {
			start := int(C.partition_info_get_node_inx(cp, C.int(2*i)))
			end   := int(C.partition_info_get_node_inx(cp, C.int(2*i+1)))
			p.NodeInx[i] = NodeRange{start, end}
		}
		//for idx := cp.node_inx; *idx != -1; idx = idx + 
		p.Nodes = C.GoString(cp.nodes)
		p.OverTimeLimit = uint16(cp.over_time_limit)
		p.PreemptMode = uint16(cp.preempt_mode)
		p.PriorityJobFactor = uint16(cp.priority_job_factor)
		p.PriorityTier = uint16(cp.priority_tier)
		p.QosChar = C.GoString(cp.qos_char)
		p.StateUp = uint16(cp.state_up)
		p.TotalCpus = uint32(cp.total_cpus)
		p.TotalNodes =  uint32(cp.total_nodes)
		p.TresFmtStr = C.GoString(cp.tres_fmt_str)
	}
	return
}
